<div style="clear: both"></div>
<div id="footer-container">
	<div class="container">
		<div class="sign-up-sec">
			<h1>Subsribe To Our News Letter</h1>
			<form action="<?php echo base_url()?>addMail" method="post">
				<input type="email" name="subs" placeholder="Enter your email ID" />
				<input type="submit" name="sbmt" value="Go" />
			</form>

			<span class="credits">Developed By <a target="_blank"
				href="http://sudosaints.com/">SudoSaints</a>
			</span>
		</div>
		<div class="footer-box">
			<h3>Help</h3>
			<ul>
				<li><a href="<?php echo site_url(); ?>how-to-order.html">How to
						order</a></li>
				<li><a href="<?php echo site_url(); ?>format-and-delivery.html">Format
						and Delivery</a></li>
				<li><a href="<?php echo site_url(); ?>payment-options.html">Payment
						Options</a></li>
				<li><a href="<?php echo site_url(); ?>">Site Map</a></li>
			</ul>
		</div>
		<div class="footer-box">
			<h3>Legal</h3>
			<ul>
				<li><a href="<?php echo site_url();?>privacy-policy.html">Privacy
						Policy</a></li>
				<li><a href="<?php echo site_url();?>disclaimer.html">Disclaimer</a>
				</li>
				<li><a href="<?php echo site_url();?>terms-and-conditions.html">Terms
						and Conditions</a></li>
				<li><a href="<?php echo site_url();?>return-policy.html">Return
						Policy</a></li>
			</ul>
		</div>
		<div class="footer-box">
			<h3>Connect With Us</h3>
			<ul>
				<li>Google Plus</li>
				<li>Facebook</li>
				<li>Linked In</li>
				<li>Twitter</li>
			</ul>
		</div>
		<div class="footer-box">
			<h3>Find Report By</h3>
			<ul>
				<li>Niche Market</li>
				<li>Upcoming Reports</li>
				<li>Company</li>
				<li>Site Map</li>
			</ul>
		</div>

	</div>
</div>

</body>
</html>
