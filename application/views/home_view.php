<?php //print_r($popularReports);die();?>
<div class="container"
	style="box-shadow: 0px 0px 2px #ccc; background: #fff;">

	<div class="section-one">
		<div class="top-desc">
			<h1>
				<?php echo $title;?>
			</h1>
			<br>
			<p>
				<?php echo $homeContent;?>
				<?php //echo substr($homeContent,0,500);?>
			</p>
		</div>
		<div style="width: 48%; padding: 10px; float: left">
			<img src="images/banner-img.jpg" width="590" height="340" />
		</div>
	</div> 

	<div id="section-two">
		<div class="box">
			<div class="box-head">
				<h1>Latest Research Reports</h1>
				<p>10th July 2013</p>
			</div>
			<?php foreach($latestReports as $report) {?>
			<?php //print_r($report);?>
			<div class="list-item">
				<p>
					<!-- <a href="<?php echo base_url().'report/view/'.$report['id'].'/'.url_title($report['report_title'],'-');?>.html"><?php echo $report['report_title']?></a> -->
					<a href="<?php echo base_url().url_title($report['report_title'],'-');?>.html"><?php echo $report['report_title']?></a>
					
				</p>
				<div class="categ">
					<p>
						Category :
						<?php echo $report['categName'];?>
						| Published Date : <?php echo $report['report_date'];?>
					</p>
				</div>
			</div>
			<?php } ?>
			<div class="bottom-more">
				<h1><a href="<?php echo base_url();?>latest">Read More...</a></h1>
			</div>
		</div>
		<div class="box">
			<div class="box-head">
				<h1>Popular Research Reports</h1>
				<p>10th July 2013</p>
			</div>
			<?php $count=0; foreach ($popularReports as $popularReport){?>
			<div class="list-item">
				<p><a href="<?php echo base_url().url_title($popularReport['report_title'],'-');?>.html"><?php echo $popularReport['report_title'];?></a></p>
				<div class="categ">
					<p>Category : <?php echo $popularReport['categName'];?> | Published Date : <?php echo $popularReport['report_date'];?></p>
				</div>
			</div>
			<?php
				$count++;
				if ($count == 6) break; 
				?>
			<?php }?>
			<div class="bottom-more">
				<h1><a href="<?php echo base_url();?>getPopularReports">Read More...</a></h1>
			</div>
		</div>
		<div class="box">
			<div class="box-head">
				<h1>Popular Categories</h1>
				<p>10th July 2013</p>
			</div>
			<?php $i=0; ?>
			<?php foreach($categs as $categ) {?>
			<div class="list-categ">
				<p><?php echo $categ['categName'];?></p>
				<div class="categ">
				</div>
			</div>
			<?php 
				$i++;
				if($i == 10) break;
			?>
			<?php } ?>
			
		</div>
		<div style="clear: both"></div>
		<div style="width: 98%; height: 150px; margin: 10px auto;">
			<div id="pop-pub">
				<h3>Popular Publishers</h3>
			</div>
			<div style="clear: both"></div>
			<div id="pub-logos">
				<?php  foreach ($publishers as $image){ ?>
				<div style="width: 197px; height: 120px; box-shadow: 0px 0px 2px #000; float: left" >
					<img alt="" src="<?php echo base_url()."admin/".$image['publisher_image'];?>" height="100%" width="100%">
					
				</div>
				<?php }?>
			</div>
		</div>

	</div>

</div>