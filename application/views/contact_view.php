<div class="container" style="box-shadow:0px 0px 2px #ccc;background:#fff;">
	<div class="contact-content">
		<div class="latest-report-title">
			<h2>Contact Us</h2>
		</div>
		<div class="about-us">
			<div class="map">
				<iframe width="970" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Chandan+Nagar,+Kharadi,+Pune,+Maharashtra&amp;aq=1&amp;oq=chandan&amp;sll=18.508702,73.812498&amp;sspn=0.071543,0.132093&amp;ie=UTF8&amp;hq=&amp;hnear=Chandan+Nagar,+Kharadi,+Pune,+Maharashtra&amp;t=m&amp;z=14&amp;ll=18.547206,73.940391&amp;output=embed"></iframe>
			</div>
			<div class="clear"></div>
			<div class="contact-form">
				<form>
					<table>
						<tr>
							<td>
								Name:
							</td>
							<td>
								<input type="text" name="name" placeholder="Enter Your Name"/>
							</td>
						</tr>
						<tr>
							<td>
								Phone Number:
							</td>
							<td>
								<input type="phone" name="phno" placeholder="Enter Your Phone Number"/>
							</td>
						</tr>
						<tr>
							<td>
								Email ID:
							</td>
							<td>
								<input type="email" name="email" placeholder="Enter Your Email ID"/>
							</td>
						</tr>
						<tr>
							<td>
								Message:
							</td>
							<td>
								<textarea placeholder="Enter Your Message"></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="submit" value="Send"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div class="address-sec">
				<table>
					<tr>
						<td>
							<b>Address:</b>
						</td>
						<td>
							Office Number 11<br>
							Street Name<br>
							City Name<br>
							State name
						</td>
					</tr>
					<tr>
						<td>
							<b>Contact Number:</b>
						</td>
						<td>
							(+91) 9876543210<br>
							(020) 34567890
						</td>
					</tr>
					<tr>
						<td>
							<b>Email ID's:</b>
						</td>
						<td>
							example@example.com<br>
							example2@example.com
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div style="clear:both"></div>