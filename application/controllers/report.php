<?php
class Report extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('pagination');
		$this->load->model('categoryModel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('news_model','news');
		$this->load->model('route_model','data');
		
	}
	public function index(){
		
	}
	
	/* public function view($id=NULL,$slug="") {
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		if(!isset($id)){
			header("Location: ".base_url());
		}
		
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$reportData = $this->report->getReport($id);
		
		$this->load->view('single_view',array('data'=>$reportData,'latest'=>$latest,'upcoming'=>$upcoming));
		
		$this->load->view('footer_view');
	} */
	
	function view(){
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$title = $this->uri->segment(1);
		//echo $title;
		//Get page details from model
		$page = $this->data->getPageDetailsByTitle($title);
		
		if(isset($page['report'])){
			$latest = $this->report->getLatest(4);
			$upcoming = $this->report->getUpcomingReports(4);
			$this->load->view('single_view',array('data'=>$page['report'][0],'latest'=>$latest,'upcoming'=>$upcoming));
		} else {
			//News
			$latest = $this->report->getLatest(4);
			$upcoming = $this->report->getUpcomingReports(4);
			//$this->load->view('single_view',array('data'=>$page['report'][0],'latest'=>$latest,'upcoming'=>$upcoming));
			$this->load->view('news_single_view',array('news'=>$page['news'][0],'latest'=>$latest,'upcoming'=>$upcoming));
		}
		
		//$this->report->
		$this->load->view('footer_view');
		
	}
	
	function searchReport(){
		$data = $this->report->reportSearch($_POST['searchValue']);
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('list_view',array('result' => $data,'latest'=>$latest,'upcoming'=>$upcoming,'name' => $_POST['searchValue']));
		$this->load->view('footer_view');
	}
	
	function viewReport(){
		$reportName = $_GET['report_name'];
		$this->report->updateCount($_GET['id']);
		header("Location:".base_url().url_title(($reportName),'-')."."."html");
	}
	
	function getPopularReports(){
		$nomRows = $this->report->getTotalRows();
		$config['base_url'] = base_url().'getPopularReports.html';
		$config['total_rows'] = $nomRows;
		$config['per_page'] = 7;
		$config['num_links'] = 2;
		$config['uri_segment'] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['num_tag_open'] ="<div class='page-no'>";
		$config['num_tag_close'] = "</div>";
		$config['cur_tag_open'] = "<div class='page-no'><b>";
		$config['cur_tag_close'] = "</b></div>";
		$config['next_tag_open'] = "<div class='page-no'>";
		$config['next_tag_close'] = "</div>";
		$config['prev_tag_open'] = "<div class='page-no'>";
		$config['prev_tag_close'] = "</div>";
		
		$this->pagination->initialize($config);
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$popularReports = $this->report->getPopularReports($config['per_page'] = 7,$this->uri->segment(2));
		$reports = $this->report->getLatest(4);
		
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('popularReports_view',array('reports' => $reports,'latest' => $popularReports,'upcoming' => $upcoming));
		$this->load->view('footer_view');
	}
	
}