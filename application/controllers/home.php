<?php
class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('home_model','home');
		$this->load->model('categoryModel','categ');
		$this->load->model('reportmodel','reports');
	}

	function index(){

		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		//$head = $this->categ->getCategsWithParents();
		$publisher = $this->home->getPublishers();

		$content = $this->home->get();
		$subcategory = $this->categ->getcategory();
		$reports = $this->reports->getLatest();
		$popularReports = $this->reports->getPopularReportsHome();
		$data = array(
						'title'=> $content['title'],
						'homeContent' => $content['content'],
						'latestReports' => $reports,
						'categs'=>$subcategory
					);
		$data['publishers'] = $publisher;
		$data['popularReports'] = $popularReports;
		$this->load->view('home_view',$data);

		$this->load->view('footer_view');
	}
	
	function addMail(){
		$this->home->add($_POST['subs']);
	}
}