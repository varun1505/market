<?php
class Legal extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('categorymodel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('legal_model','legal');
	}
	
	function privacyPolicy(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->legal->getPrivacyPolicy();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function disclaimer(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->legal->getDisclaimer();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function termsAndConditions(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->legal->getTermsAndConditions();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function returnPolicy(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->legal->getReturnPolicy();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
}