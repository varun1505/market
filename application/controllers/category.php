<?php
class Category extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('categoryModel','category');
		$this->load->model('reportmodel','report');
	}
	
	function index(){
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$head = $this->category->getCategsWithParents();
		$data = $this->category->getCategs();
		$this->load->view('header_view',array('latest'=>$latest,'categs' => $data,'upcoming'=>$upcoming));
		$this->load->view('category_view',array('categs'=> $head));
		$this->load->view('footer_view');
	}
	
	function searchReportByCatgs(){
		$categName = $_GET['name'];
		$data = $this->report->reportSearch($categName);
		$head = $this->category->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('subCategorySearch_view',array('result' => $data,'latest'=>$latest,'upcoming'=>$upcoming,'name' => $categName));
		$this->load->view('footer_view');
	}
	
	function getSubCategories(){
		$categName = $_GET['category'];
		$head = $this->category->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$data = $this->category->subCategorySearch($categName);
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('subcategoryDisplay_view',array('result' => $data,'latest'=>$latest,'upcoming'=>$upcoming,'name' => $categName));
		$this->load->view('footer_view');
	}
}