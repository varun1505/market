<?php
class Help_model extends CI_Model{
	function getHowToOrder(){
		$this->db->select('*');
		$this->db->where('url','how-to-order.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
	
	function getFormatAndDelivery(){
		$this->db->select('*');
		$this->db->where('url','format-and-delivery.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
	
	function getPaymentOptions(){
		$this->db->select('*');
		$this->db->where('url','payment-options.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
}