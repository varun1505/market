<?php
class News_model extends CI_Model {
	
	function getAll($limit=8){
		$this->db->select('*');
		$this->db->limit($limit);
		$result = $this->db->get('news');
		$result = $result->result_array();
		return $result;
	}
	
	function getNewsById($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$result = $this->db->get('news');
		if($result->num_rows) {
			return $result->result_array();
		} 
		return false;
	}
}