<?php
class Legal_model extends CI_Model {
	function getPrivacyPolicy(){
		$this->db->select('*');
		$this->db->where('url','privacy-policy.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
	
	function getDisclaimer(){
		$this->db->select('*');
		$this->db->where('url','disclaimer.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
	
	function getTermsAndConditions(){
		$this->db->select('*');
		$this->db->where('url','terms-and-conditions.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
	
	function getReturnPolicy(){
		$this->db->select('*');
		$this->db->where('url','return-policy.html');
		$data = $this->db->get('pages');
		$data = $data->result_array();
		return $data[0];
	}
}