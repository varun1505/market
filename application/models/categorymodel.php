<?php
class categoryModel extends CI_Model{
	
	function getCategs(){
		$this->db->select('*');
		$query = $this->db->get('parent_category');
		
		if( $query->num_rows > 0 ){
			return $query->result_array();
		} else {
			return 0;
		}
	}
	
	function subCategorySearch($categ){
		$this->db->select('*');
		$this->db->where('parentCateg_name',$categ);
		$this->db->limit(7);
		$this->db->join('category','parent_category.parentCategId = category.parentCategId');
		$query = $this->db->get('parent_category');
		$results = $query->result_array();
		return $results;
	}
	
	function getCategsWithParents(){
		$this->db->select('*');
		$this->db->join('parent_category','category.parentCategId =parent_category.parentCategId');
		$query = $this->db->get('category');
		$results = $query->result_array();
		
		$final = array();
		
		$categories = array();
		
		foreach($results as $result) {
			if(isset($categories[$result['parentCateg_name']])){
				$temp = $categories[$result['parentCateg_name']];
			} else {
				$temp = array();
			}
			$categ = array();
			$categ['categName'] = $result['categName'];
			$categ['categId'] = $result['categId'];
			$temp[] = $categ;
			$categories[$result['parentCateg_name']] =  $temp;
		}
		
		/* echo "<pre>";
		print_r($categories);
		die();
		 */
		return $categories;
	}
	/*  function getCategsWithParents(){
		$this->db->select('*');
		$query = $this->db->get('category');
		
		if( $query->num_rows > 0 ){
			$categs = $query->result();
			
			$final = array();
			foreach($categs as $categ){
				$temp = array();
				
				$temp['categId'] = $categ->categId;
				$temp['categName'] = $categ->categName;
				$id = $temp['categId'];
				
				$this->db->select('*');
				$this->db->from('parent_category');
				$this->db->join('category', 'category.categId = parent_category.parentCategId');
				$this->db->where('parent_category.parentCategId',$id);
				$parents = $this->db->get();
				if($parents->num_rows > 0){
					$parents = $parents->result();
					$parent = $parents[0];
					$temp['parentCategId'] = $parent->parentCategId;
					$temp['categParent'] = $parent->categName;
				} else {
					$temp['categParentId'] = false;
					$temp['categParent'] = false;
				}
				$final[] = $temp;
			}
		} else {
			return 0;
		}
	
		return $final;	
	}  */
	
	function getcategory(){
		$query = $this->db->get('category');
		$row=$query->result_array();
		return $row;
	}
}