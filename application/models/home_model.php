<?php
class Home_model extends CI_Model{
	function get(){
		$this->db->select('title,content');
		$this->db->from('home');
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$result = $query->result_array();
			return $result[0];
		} 
		return array('title'=>'','content'=>'');
	}
	
	function getPublishers(){
		$this->db->select('publisher_image');
		$this->db->limit(5);
		$q = $this->db->get('publishers');
		return $q->result_array();
	}
	
	function add($email){

		$data = array(
				'email' => $email
				);
		$this->db->insert('subscribers',$data);
		header("Location:".base_url());
	}
}