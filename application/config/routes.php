<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

//$route['report/:num/:any'] = 'report/$1/$2';


//$route['news/(:any)'] = 'news/view/$1';
/* $route['news.html'] = 'news/';
$route['categories.html'] = 'category/';
$route['latest.html'] = 'latest/';
$route['about-us.html'] = 'page/about';
 */
$route['categories'] = 'category';
$route['searchReportByCatgs'] = 'category/searchReportByCatgs';
$route['getSubCategories'] = 'category/getSubCategories';

$route['news'] = 'news';
//$route['categories'] = 'categories/view';
$route['about-us'] = 'page/about';
$route['contact'] = 'page/contact';

//Routes for Help footer section
$route['how-to-order'] = 'help/howToOrder';
$route['format-and-delivery'] = 'help/formatAndDelivery';
$route['payment-options'] = 'help/paymentOptions';

//Routes for Legal Footer Section
$route['privacy-policy'] = 'legal/privacyPolicy';
$route['disclaimer'] = 'legal/disclaimer';
$route['terms-and-conditions'] = 'legal/termsAndConditions';
$route['return-policy'] = 'legal/returnPolicy';

$route['addMail'] ='home/addMail';

$route['latest'] = 'page/latest';
$route['latest.html/(:num)'] = 'page/latest';

$route['searchReport'] = 'report/searchReport';
$route['viewReport'] = 'report/viewReport';
$route['getPopularReports'] = 'report/getPopularReports';
$route['getPopularReports.html/(:num)'] = 'report/getPopularReports';

// $route['page/(:any)'] = 'page/view/$1';
$route['(:any)'] = 'report/view/$1';  // For both news and Reports..


/* End of file routes.php */
/* Location: ./application/config/routes.php */