<?php
class Upcoming extends CI_Controller{
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('upcoming_model','upcoming');
		$this->load->model('categorymodel','categ');
	}

	public function index(){
		$this->load->view('header_view');
		
		$data= $this->upcoming->getReports();
		$this->load->view('displayUpcoming_view',array('data' =>$data));

		$this->load->view('footer_view');
	}

	public function add(){

		$this->load->view('header_view');
		$data= $this->categ->getcategory();
		$this->load->view('addUpcoming_view',array('data' =>$data));
		$this->load->view('footer_view');

	}

	public function addReport(){
		
		$this->upcoming->add($_POST);
		header("Location:".base_url()."upcoming");
	}
	
	function delete(){
		$id=$_GET['id'];
		$this->load->model('reportmodel');
		$this->reportmodel->delete($id);
		header("Location: ".base_url()."upcoming");
	}
	
	function edit(){
		$id=$_GET['id'];	
		$this->load->view('header_view');
		
		$report = $this->upcoming->getReport($id);
		$data= $this->categ->getcategory();					
		$this->load->view('addUpcoming_view',array('report'=>$report,'data' =>$data));
		$this->load->view('footer_view');
	}
	
	function update(){
		
		$this->upcoming->editReport($_POST);
		
		header("Location: ".base_url()."upcoming");
		
	}
}