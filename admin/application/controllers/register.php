<?php
class Register extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('register_model','register');
	}
	
	public function index(){
		$this->load->view('register_view');
	}
	
	function addUser(){
		$this->register->addUser($_POST);
		
		/* print_r($_POST);
		die(); */
	}
	
}