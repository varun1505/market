<?php
class Parentcategory extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('parent_model','parent');
		$this->load->model('categorymodel','categ');
	}
	
	function index(){
		$parentCategs = $this->parent->getParentsCatgs();
		$this->load->view('header_view');
		$this->load->view('parenCategList_view',array('categs' => $parentCategs));
		$this->load->view('footer_view');
	}
	
	function add(){
		$this->load->view('header_view');
		$this->load->view('addParentCateg_view');
		$this->load->view('footer_view');
	}
	
	function addParent(){
		$this->parent->addParent($_POST['parentcategName']);
		header("Location:".base_url()."parentcategory");
	}
	
	function delete(){
		$this->parent->delete($_GET['id']);
		header("Location:".base_url()."parentcategory");
	}
}