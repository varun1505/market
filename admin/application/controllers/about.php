<?php
class About extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('about_model');
	}
	
	function index(){
		$this->load->view('header_view');
		$about = $this->about_model->getAbout();
		
		$this->load->view('about_view',array('content'=>$about['content']));
		$this->load->view('footer_view');
	}
	
	function save(){
		
		$this->about_model->saveAbout($_POST['about']);
		header("Location: ".base_url().'about/');
	}
	
	/* function getAbout(){
		$this->about_model->getAbout();
	} */
	
	
	
}
?>