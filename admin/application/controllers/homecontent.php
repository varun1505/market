<?php
class Homecontent extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('home_model','home');
	}
	
	public function index(){
		$this->load->view('header_view');
		
		$temp = $this->home->get();
		$data = array('title'=>$temp['title'] ,'content'=>$temp['content']);
		$this->load->view('homecontent_view',$data);
		
		$this->load->view('footer_view');
	}
	
	public function save(){
		/* $content = $_POST['homecontent'];
		$title */
		$this->home->saveContent($_POST);
		header("Location: ".base_url()."homecontent");
	}
	
	public function getContent(){
		
		$content = $this->home->get();
	}
	
}