<?php
class Report extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function index(){
		$this->load->view('header_view');
		$this->load->model('reportmodel');
		$data= $this->reportmodel->getReports();
		$this->load->view('displayReport_view',array('data' =>$data));

		$this->load->view('footer_view');
	}

	public function add(){

		$this->load->view('header_view');

		$this->load->model('categorymodel');
		$data= $this->categorymodel->getcategory();
		$this->load->view('addReport_view',array('data' =>$data));
		$this->load->view('footer_view');

	}

	public function addReport(){

		$this->load->view('header_view');
		$files="";
		$allowedExts = array("pdf");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (in_array($extension, $allowedExts)){

			if (file_exists("uploads/" . $_FILES["file"]["name"]))
			{
				//renaming the existing file
				$filename= $_FILES["file"]["name"];
				$removeextention = str_replace(".pdf", "", $filename);
				$shuffled = str_shuffle($removeextention).".pdf";
				
				// This will echo something like: bfdaec
				move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $shuffled);
				$files="uploads/" . $shuffled;
				
			}
			else
			{
				move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $_FILES["file"]["name"]);
				$files="uploads/" . $_FILES["file"]["name"];
			}
		}
		else {
			echo "Invalid File";die();
		}
		
		$this->load->model('reportmodel');
		$this->reportmodel->add($_POST,$files);
		
		$this->load->view('footer_view');
	}
	
	function delete(){
		$id=$_GET['id'];
		$this->load->model('reportmodel');
		$this->reportmodel->delete($id);
		header("Location: ".base_url()."report");
		
	}
	
	function edit(){
		$id=$_GET['id'];	
		$this->load->view('header_view');
		$this->load->model('reportmodel');
		$this->load->model('categorymodel');
		
		$report = $this->reportmodel->getReport($id);
		$data= $this->categorymodel->getcategory();					
		$this->load->view('addReport_view',array('report'=>$report,'data' =>$data));
		$this->load->view('footer_view');
	}
	
	function updateReport(){
		
		if($_FILES["file"]["name"] != ""){
			$files="";
			$allowedExts = array("pdf");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			
			if (in_array($extension, $allowedExts)){
			
				if (file_exists("uploads/" . $_FILES["file"]["name"]))
				{
					echo $_FILES["file"]["name"] . " already exists. ";
				}
				else
				{
					move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $_FILES["file"]["name"]);
					$files="uploads/" . $_FILES["file"]["name"];
				}
			}
			else {
				echo "Invalid File";die();
			}	
		}
		
		
		$this->load->model('reportmodel');
		$this->reportmodel->editReport($_POST);
		
		header("Location: ".base_url()."report");
		
	}	
}