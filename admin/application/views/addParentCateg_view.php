<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Add Parent Category
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo base_url();?>parentcategory/addParent"
				method="POST">
				<fieldset>
					<legend>Add Parent Category</legend>

					<label>Parent Category name</label> <input type="text"
						placeholder="Parent Category Name" name="parentcategName"> <span
						class="help-block">Enter Parent Category Name. Eg:Information Technology, Business & Finance,
						Energy & Transport, etc.</span>
					<input type="submit" class="btn" value="Submit" />
				</fieldset>
			</form>

		</div>
	</div>
	<!-- content ends -->
</div>


