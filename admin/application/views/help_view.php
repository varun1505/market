<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<script type="text/javascript">
			$(document).ready(function(){
				$(".text-area").htmlarea();
			}); 
		</script>
		<style>
			.jHtmlArea, .jHtmlArea iframe, .jHtmlArea .ToolBar{
				width: 100% !important;
			}
		</style>
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Help
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo base_url();?>help/save" method="POST">
				<h3>How To Order</h3>
				<textarea class="text-area" name="how-to-order" id="how-to-order" rows="10" cols="100"><?php
					echo $data[0]['content']; 
				?></textarea>
				<hr/>
				<h3>Format and Delivery</h3>
				<textarea class="text-area" name="format-and-delivery" id="format-and-delivery" rows="10" cols="100"><?php
					echo $data[1]['content']; 
				?></textarea>
				<hr/>
				<h3>Payment Options</h3>
				<textarea class="text-area" name="payment-options" id="payment-options" rows="10" cols="100"><?php
					echo $data[2]['content']; 
				?></textarea>
				<hr/>
				<!-- <h3>Site Map</h3>
				<textarea class="text-area" name="about" id="about" rows="10" cols="100"></textarea> -->
				<button type="submit" class="btn btn-success">Save</button>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>
