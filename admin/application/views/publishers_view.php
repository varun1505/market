<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Categories
			</h2>

		</div>
		<div class="box-content">

			<?php if($values == 0) {?>
			<p class="lead well" align="center">
				No categories added yet. You can add a category <a href="#">here</a>.
			</p>
			<?php } else {?>
			<table
				class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
				<tr>
					<th>#</th>
					<th>publisher Name</th>
					<th>Image</th>
				</tr>
				</thead>
				<tbody>
				<?php $i=1; foreach ($values as $data) {?>
				<tr>
					<td> <?php echo $i++;?>
					</td>
					<td><?php echo $data['publisher_name']; ?>
					</td>
					<td><img src="<?php echo $data['publisher_image']; ?>" height="50" width="50">
					</td>
				</tr>
				<?php }?>
				</tbody>
			</table>
			<?php } ?>
			
		</div>
	</div>
	<!-- content ends -->
</div>