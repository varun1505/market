<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<script type="text/javascript">
			$(document).ready(function(){
				$("#about").htmlarea();
			});
		</script>
		<style>
			.jHtmlArea, .jHtmlArea iframe, .jHtmlArea .ToolBar{
				width: 100% !important;
			}
		</style>
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>About Us
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo base_url();?>/about/save" method="POST">
				<textarea class="homecontent" name="about" id="about" rows="10" cols="100"><?php
					echo $content; 
				?></textarea>
				<button type="submit" class="btn btn-success">Save</button>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>
