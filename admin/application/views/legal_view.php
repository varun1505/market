<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<script type="text/javascript">
			$(document).ready(function(){
				$(".text-area").htmlarea();
			});
		</script>
		<style>
.jHtmlArea,.jHtmlArea iframe,.jHtmlArea .ToolBar {
	width: 100% !important;
}
</style>
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Legal
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo base_url();?>/legal/save" method="POST">
				<h3>Privacy Policy</h3>
				<textarea class="text-area" name="privacy-policy" id="privacy-policy" rows="10"
					cols="100">
					<?php
					echo $data[0]['content'];
					?></textarea>
				<hr />
				<h3>Disclaimer</h3>
				<textarea class="text-area" name="disclaimer" id="disclaimer" rows="10"
					cols="100">
					<?php
					echo $data[1]['content'];
					?></textarea>
				<hr />
				<h3>Terms and Conditions</h3>
				<textarea class="text-area" name="terms-and-conditions" id="terms-and-conditions" rows="10"
					cols="100">
					<?php
					echo $data[2]['content'];
					?></textarea>
				<hr />
				
				<h3>Return Policy</h3>
				<textarea class="text-area" name="return-policy" id="return-policy" rows="10"
					cols="100">
					<?php
					echo $data[3]['content'];
					?></textarea>
				<hr />
				
				<button type="submit" class="btn btn-success">Save</button>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>
