<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Categories
			</h2>

		</div>
		<div class="box-content">

			<?php if($categs == 0) {?>
			<p class="lead well" align="center">
				No categories added yet. You can add a category <a href="#">here</a>.
			</p>
			<?php } else {?>
			<table
				class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
				<tr>
					<th>#</th>
					<th>Category Name</th>
					<th>Parent Category</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($categs as $categ) {?>
				<tr>
					<td><?php echo $categ->categId; ?>
					</td>
					<td><?php echo $categ->categName; ?>
					</td>
					<td><?php  echo $categ->parentCateg_name; ?>
					</td>
				</tr>
				<?php }?>
				</tbody>
			</table>
			<?php } ?>
			
		</div>
	</div>
	<!-- content ends -->
</div>

