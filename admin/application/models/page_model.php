<?php
class Page_model extends CI_Model {
	
	function saveHelp($data){
		
		$howToOrder = $data['how-to-order'];
		$formatAndDelivery = $data['format-and-delivery'];
		$paymentOptions = $data['payment-options'];
		
		//Save each separately
		
		//Save How-to-Order
		$this->db->where('url','how-to-order.html');
		$this->db->delete('pages');
		
		$data = array(
					'type' => 3,
					'title' => 'How To Order',
					'content' => $howToOrder,
					'url' => 'how-to-order.html'
				);
		$this->db->insert('pages',$data);
		
		//Save Format-and-Delivery
		$this->db->where('url','format-and-delivery.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Format and Delivery',
				'content' => $formatAndDelivery,
				'url' => 'format-and-delivery.html'
		);
		$this->db->insert('pages',$data);
		
		//Save Payment-Options
		$this->db->where('url','payment-options.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Payment Options',
				'content' => $paymentOptions,
				'url' => 'payment-options.html'
		);
		$this->db->insert('pages',$data);
		
	}
	
	function getHelp(){
		$this->db->select('*');
		$this->db->where('url','how-to-order.html');
		$this->db->or_where('url','format-and-delivery.html');
		$this->db->or_where('url','payment-options.html');
		$help = $this->db->get('pages');
		if($help->num_rows() > 0){
			return $help->result_array();
		} else {
			return false;
		}
	}
	
	
	function saveLegal($data){
		/* echo "<pre>";
		print_r($data);
		die(); */
		
		$privacyPolicy = $data['privacy-policy'];
		$disclaimer = $data['disclaimer'];
		$termsAndConditions = $data['terms-and-conditions'];
		$returnPolicy = $data['return-policy'];
		
		//Save Privacy-Policy
		$this->db->where('url','privacy-policy.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Privacy Policy',
				'content' => $privacyPolicy,
				'url' => 'privacy-policy.html'
		);
		$this->db->insert('pages',$data);
		
		//Save Disclaimer
		$this->db->where('url','disclaimer.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Disclaimer',
				'content' => $disclaimer,
				'url' => 'disclaimer.html'
		);
		$this->db->insert('pages',$data);
		
		//Save Terms and conditions
		$this->db->where('url','terms-and-conditions.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Terms and Conditions',
				'content' => $termsAndConditions,
				'url' => 'terms-and-conditions.html'
		);
		$this->db->insert('pages',$data);
		
		//Save Return-Policy
		$this->db->where('url','return-policy.html');
		$this->db->delete('pages');
		
		$data = array(
				'type' => 3,
				'title' => 'Return Policy',
				'content' => $returnPolicy,
				'url' => 'return-policy.html'
		);
		$this->db->insert('pages',$data);
		
	}
	
	function getLegal(){
	$this->db->select('*');
		$this->db->where('url','privacy-policy.html');
		$this->db->or_where('url','disclaimer.html');
		$this->db->or_where('url','terms-and-conditions.html');
		$this->db->or_where('url','return-policy.html');
		$help = $this->db->get('pages');
		if($help->num_rows() > 0){
			return $help->result_array();
		} else {
			return false;
		}
	}
	
}