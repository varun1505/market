<?php
class Parent_model extends CI_Model{
	 function addParent($value){
	 	$data = array(
	 			'parentCateg_name' => $value
	 			);
	 	$this->db->insert('parent_category',$data);
	 }
	 
	 function getParentsCatgs(){
	 	$query = $this->db->get('parent_category');
	 	$row=$query->result();
	 	return $row;
	 }
	 
	 function delete($id){
	 	$tables = array('parent_category','category');
	 	$this->db->where('parentCategId',$id);
	 	$this->db->delete($tables);
	 }
}