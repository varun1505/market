<?php
class reportmodel extends CI_Model{
	public function add($value,$file){


		$data=array(
				'report_title' => $value['reportName'],
				'report_description' => $value['description'],
				'table_of_contents' => $value['tableOfContents'],
				'categId' => $value['report_category'],
				'report_file' =>$file
		);
		$this->db->insert('report',$data);
		$id = $this->db->insert_id();
		$route = array(
				'post_id' => $id,
				'post_type' => 1,
				'post_title' => url_title($data['report_title']).".html"
		);
		$this->db->insert('routes',$route);

		//pass report thins url link to subscribers
		$stringUrl = base_url().url_title($value['reportName'],'-')."."."html";
		$url = str_replace("/admin", "",$stringUrl);
		//email the subscribers
		//email Subject
		//Report Description as a Message
		$description = $value['description'];
		//get the category Name
		$this->db->select('categName');
		$this->db->where('categId',$value['report_category']);
		$query = $this->db->get('category');
		$categoryName = $query->row_array();
		//report Description as a message of email
		$message = '<html><body>';
		$message .= '<h1>'.$value['reportName'].'</h1><br>';
		$message .= '<p>'.$description.'</p><br>';
		$message .= '<p><strong>Category of '.$categoryName['categName'].'</strong></p>';
		$message .= '<p><i>You can Check this Report through below link </i><br>'.$url.'</p>';
		$message .= '</body></html>';
		
		$this->mailSubscribers($message);
		
		header("Location: ".base_url()."report");
	}
	
	//email Report to all Subscribers
	function mailSubscribers($message){
		$this->db->select('email');
		$query = $this->db->get('subscribers');
		$result = $query->result_array();
		$headers = "From: Big Market Research <no-reply@bigmarketresearch.com> \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		foreach ($result as $email){
			mail($email['email'],"New Report Notification",$message,$headers);
		}
	}

	function getReports(){
		$this->db->select('*');
		$this->db->join('category','report.categId=category.categId' );
		$query = $this->db->get('report');
		$row = $query->result();
		return $row;
	}

	function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('report');
	}

	function getReport($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$q = $this->db->get('report');
		$result = $q->result_array();
		return $result;

	}

	function editReport($value,$file=""){
		$data=array(
				'report_title' => $value['reportName'],
				'report_description' => $value['description'],
				'table_of_contents' => $value['tableOfContents'],
				'categId' => $value['report_category']
		);

		if($file != ""){
			$data['report_file'] = $file;
		}

		$this->db->where('id',$value['reportId']);
		$this->db->update('report',$data);
		/* echo $this->db->last_query();
		 echo "<br/><br/>";
		echo $this->db->affected_rows(); */

		//Delete Route and add again..

		$this->db->where('post_id',$value['reportId']);
		$this->db->where('post_type',1);
		$this->db->delete('routes');

		//Insert Updated value in routes table

		$route = array(
				'post_id' => $value['reportId'],
				'post_type' => 1,
				'post_title' => url_title($value['reportName']).".html"
		);
		$this->db->insert('routes',$route);
	}
}